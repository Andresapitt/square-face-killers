
var timeleft = 100;
var timeTotal=100;
//var contWidth=$.timerCont.width;
var countdownTimer;
var whosturn= -1; //player 1 starts after whosnext
var turns=["player1","player2","player3","player4"];
var currentBoxSelected=null;
var index=0;
var killingPhrases=["Kill","Bye", "Get", "Sayonara","Hasta la vista,","Se ya", "catch you later","so long", "ciao","ta ta","adieu", "farewell","later", "laters","Keep it real", "peace out","cheerio", "cheery-bye","toodle pip" ];


var players = {
  player1: {
  	id:"player1",
  	name:Ti.App.Properties.getString("player1_name", "Player 1"),
  	color:Ti.App.Properties.getString("player1_color", "#ff1234"),
  	lives:4,
  	turn:true
  },
  player2: {
  	id:"player2",
  	name:Ti.App.Properties.getString("player2_name", "Player 2"),
  	color:Ti.App.Properties.getString("player2_color", "#ff1234"),
  	lives:4,
  	turn:false
  },
  player3: {
  	id:"player3",
  	name:Ti.App.Properties.getString("player3_name", "Player 3"),
  	color:Ti.App.Properties.getString("player3_color", "#ff1234"),
  	lives:4,
  	turn:false
  },
  player4: {
  	id:"player4",
  	name:Ti.App.Properties.getString("player4_name", "Player 4"),
  	color:Ti.App.Properties.getString("player4_color", "#ff1234"),
  	lives:4,
  	turn:false
   }
};

console.log("total players:"+Object.keys(players).length);

function startTimer(e){
	countdownTimer = setInterval(function(){
	var progressBarWidth = timeleft * $.timerCont.width / timeTotal;
	$.timerCont.width= progressBarWidth; 
	//$.counterLabel.text = Math.round((1000 - --timeleft)/100);
	console.log("timeleft:"+Math.round(timeleft/10));
	$.counterLabel.text=Math.round(timeleft/10);
	  if(timeleft <= 0)
	    clearInterval(countdownTimer);
	},100);
}



function stopTimer(e){
	timeleft = 100;
	timeTotal=100;
	$.timerCont.width = 320;
	clearInterval(countdownTimer);
}



function startGame(e){
	//setting players name
	// addevenlistern to change button to player button
	SwapStartBtn();
	//startTimer(e);
}



/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}


function faceClicked(e){
	console.log(e.source.id);
	if(currentBoxSelected){
		currentBoxSelected.borderWidth =0;
	}
   	e.source.borderColor ="#ffffff";
	e.source.borderWidth =2;
	currentBoxSelected=e.source;
	shuffleArray(killingPhrases);
	$.voteLabel.text=killingPhrases[0]+" "+players[e.source.id]["name"];
}



function SwapStartBtn(){
	//$.startGameBtn.addEventListener("click",killPlayer);
    $.startGameBtn.visible=!$.startGameBtn.visible;
    $.voteBtn.visible=!$.voteBtn.visible; 
	whoisnext();
}



//confirm button whihc player you want to kill
function Vote(e){
	
	currentBoxSelected.borderWidth = 0;
	currentBoxSelected.backgroundColor = "#545454";
	currentBoxSelected.backgroundImage = "images/faces/face_1.png";
	
	checkLivesLeft(currentBoxSelected.id); // send which player was selected and voted out

}


//change face to death for the selected
function checkLivesLeft(player){
	//check current player lives and take one out.
	console.log("whosturn: "+whosturn);
	console.log("voted for:"+players[player]['id']);
	console.log("players lives before vote:"+players[player]["lives"]);
	players[player]["lives"]=players[player]["lives"]-1;
	console.log("==================================================");
	console.log("players lives after vote: "+players[player]["lives"]);
	if (players[player]["lives"]==0){
		console.log(player+" is dead");	
		//delete player from pool
		delete players[player]; 
		console.log(turns);
		var whichToDelete = turns.indexOf(player);
		console.log("whichonetoDelete: "+whichToDelete);
		turns.splice(whichToDelete, 1);
		console.log(turns);		
	}	
	
	    var playersLeft=Object.keys(players).length;
		console.log(playersLeft+" players left");
		if(playersLeft==1){
			playerWins();
		}	
		else{
			whoisnext();	
		}
}


function playerWins(){
	for (var key in players){
		console.log("winner:"+key);
		//alert(key + "wins");
		reset();
		var data = {
		};
        data.winner= key;
     
		var winnerWindow = Alloy.createController('party_winner', data).getView();
    	winnerWindow.open();
	}
}

//player Turn, what happens on the players turn
function PlayerTurn(){
	buttonPressed();
	whoisnext();
}

// see whos turn it is
function whoisnext(){
	whosturn++;
	console.log("next player is player:"+whosturn);
	//console.log("players left:"+Object.keys(players).length);
	
	if (whosturn>=Object.keys(players).length){
		whosturn=0; //first player
	}
	
	console.log("current player:"+turns[whosturn]);
	
	//$.voteLabel.text=players['player'+whosturn]['name']+ " VOTE NOW";
	$.voteLabel.text=players[turns[whosturn]]['name']+ " VOTE NOW";
	///setting player color to startgame button
	$.voteBtn.backgroundColor=players[turns[whosturn]]['color'];
	EnableBoxes();
	
}

function EnableBoxes(){
	switch (whosturn){
		case 0: 
		$.player_1_col.touchEnabled=false;
		$.player_2_col.touchEnabled=true;
		$.player_3_col.touchEnabled=true;
		$.player_4_col.touchEnabled=true;
		break;
		
		case 1: 
		$.player_1_col.touchEnabled=true;
		$.player_2_col.touchEnabled=false;
		$.player_3_col.touchEnabled=true;
		$.player_4_col.touchEnabled=true;
		break;
		
		case 2: 
		$.player_1_col.touchEnabled=true;
		$.player_2_col.touchEnabled=true;
		$.player_3_col.touchEnabled=false;
		$.player_4_col.touchEnabled=true;
		break;
		
		case 3: 
		$.player_1_col.touchEnabled=true;
		$.player_2_col.touchEnabled=true;
		$.player_3_col.touchEnabled=true;
		$.player_4_col.touchEnabled=false;
		break;
		
	}
}

function reset(){
	
//variables

timeleft = 100;
timeTotal=100;
//contWidth=$.timerCont.width;

whosturn= -1; //player 1 starts after whosnext
turns=["player1","player2","player3","player4"];
currentBoxSelected=null;
index=0;

players = {
  player1: {
  	id:"player1",
  	name:Ti.App.Properties.getString("player1_name", "Player 1"),
  	color:Ti.App.Properties.getString("player1_color", "#ff1234"),
  	lives:4,
  	turn:true
  },
  player2: {
  	id:"player2",
  	name:Ti.App.Properties.getString("player2_name", "Player 2"),
  	color:Ti.App.Properties.getString("player2_color", "#ff1234"),
  	lives:4,
  	turn:false
  },
  player3: {
  	id:"player3",
  	name:Ti.App.Properties.getString("player3_name", "Player 3"),
  	color:Ti.App.Properties.getString("player3_color", "#ff1234"),
  	lives:4,
  	turn:false
  },
  player4: {
  	id:"player4",
  	name:Ti.App.Properties.getString("player4_name", "Player 4"),
  	color:Ti.App.Properties.getString("player4_color", "#ff1234"),
  	lives:4,
  	turn:false
   }
};


	
//create boxes
	for (var i=0;i<4;i++){
		
		for(var j=0;j<4;j++){
			var view = Titanium.UI.createView({
			   backgroundColor:'#ff1234',
			   width:75,
			   height:75,
			   bottom:(80*j)
			});
			
			view.addEventListener("click",faceClicked);
			//view.touchEnabled=false;
			
			switch (i){
				case 0:
				view.id="player1";
				view.backgroundColor=Ti.App.Properties.getString("player1_color", "#ff1234");
				view.backgroundImage=Ti.App.Properties.getString("player1_face", "#ff1234");
				$.player_1_col.add(view);
				
				break;
				
				case 1:
				view.id="player2";
				view.backgroundColor=Ti.App.Properties.getString("player2_color", "#ff1234");
				view.backgroundImage=Ti.App.Properties.getString("player2_face", "#ff1234");
				$.player_2_col.add(view);
				break;
				
				case 2:
				view.id="player3";
				view.backgroundColor=Ti.App.Properties.getString("player3_color", "#ff1234");
				view.backgroundImage=Ti.App.Properties.getString("player3_face", "#ff1234");
				$.player_3_col.add(view);
				break;
				
				case 3:
				view.id="player4";
				view.backgroundColor=Ti.App.Properties.getString("player4_color", "#ff1234");
				view.backgroundImage=Ti.App.Properties.getString("player4_face", "#ff1234");
				$.player_4_col.add(view);
				break;		
			}
		}
	}
}
reset();