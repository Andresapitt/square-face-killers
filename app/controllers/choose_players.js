//parameters from previous window
//var args = arguments[0] || {};

//for when coming back form choosing colors
$.choose_players.addEventListener("focus", function(e){
	$.face_1.setBackgroundColor(Ti.App.Properties.getString("player1_color", "#3A3A3A"));
	$.face_1.setBackgroundImage(Ti.App.Properties.getString("player1_face", ""));
	$.player1.text= Ti.App.Properties.getString("player1_name", "Player 1");
	
	$.face_2.setBackgroundColor(Ti.App.Properties.getString("player2_color", "#3A3A3A"));
	$.face_2.setBackgroundImage(Ti.App.Properties.getString("player2_face", ""));
	$.player2.text= Ti.App.Properties.getString("player2_name", "Player 2");
	
	$.face_3.setBackgroundColor(Ti.App.Properties.getString("player3_color", "#3A3A3A"));
	$.face_3.setBackgroundImage(Ti.App.Properties.getString("player3_face", ""));
	$.player3.text= Ti.App.Properties.getString("player3_name", "Player 3");
	
	$.face_4.setBackgroundColor(Ti.App.Properties.getString("player4_color", "#3A3A3A"));
	$.face_4.setBackgroundImage(Ti.App.Properties.getString("player4_face", ""));
	$.player4.text= Ti.App.Properties.getString("player4_name", "Player 4");
});


function chooseCharacter(e,player,id) {
	var name = "";
	switch(id){
		case "player1":
		name=$.player1.text;
		break;
		
		case "player2":
		name=$.player2.text;
		break;
		
		case "player3":
		name=$.player3.text;
		break;
		
		case "player4":
		name=$.player4.text;
		break;
	}
	var data = {
	};
        data.player= player;
        data.id= id;
        data.name= name;
	var chooseCharacter = Alloy.createController('choose_character', data).getView();
    chooseCharacter.open();
}

function gameWindow(e){
	var gameWindow = Alloy.createController('game').getView(); 
    gameWindow.open();
}



$.bt_player1.addEventListener('click', function(e) {
  chooseCharacter(e,"Player 1","player1");
});

$.bt_player2.addEventListener('click', function(e) {
  chooseCharacter(e,"Player 2","player2");
});

$.bt_player3.addEventListener('click', function(e) {
  chooseCharacter(e,"Player 3","player3");
});

$.bt_player4.addEventListener('click', function(e) {
  chooseCharacter(e,"Player 4","player4");
});

//var grid = require('gridlines');
//grid.drawgrid(20,$.home);