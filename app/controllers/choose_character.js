//parameters from previous window
var args = arguments[0] || {};
var previousFace=null;
var previousColor=null;


//console.log("name="+args.id);
$.playerLabel.value=args.name;

function chooseColor(e) {
	
	if(previousColor){
		previousColor.borderWidth=0;
	}
	e.source.borderColor="#ffffff";
	e.source.borderWidth=2;
	previousColor=e.source;
	
	var color = e.source.getBackgroundColor();
	$.characterFace.setBackgroundColor(color);
	switch (args.id){
		case "player1": Ti.App.Properties.setString("player1_color", color);
		break;
		
		case "player2": Ti.App.Properties.setString("player2_color", color);
		break;
		
		case "player3": Ti.App.Properties.setString("player3_color", color);	
		break;
		
		case "player4": Ti.App.Properties.setString("player4_color", color);
		break;
	}
}

function changeFace(e){
	var face = e.source.getBackgroundImage();
	if(previousFace){
		previousFace.borderWidth=0;
	}
	e.source.borderColor="#ffffff";
	e.source.borderWidth=2;
	previousFace=e.source;
	$.characterFace.setBackgroundImage(face);
	console.log("id:"+args.id);
	
	switch (args.id){
		case "player1": 
		Ti.App.Properties.setString("player1_face", face);
		break;
		
		case "player2": 
		Ti.App.Properties.setString("player2_face", face);
		break;
		
		case "player3": 
		Ti.App.Properties.setString("player3_face", face);
		break;
		
		case "player4":
		Ti.App.Properties.setString("player4_face", face);
		break;
	}
}

function startWindow(e){
	var gameWindow = Alloy.createController('game').getView(); 
    gameWindow.open();
}


$.closeBtn.addEventListener('click', function(e) {
 $.choose_character.close();
});

$.bt_choose.addEventListener('click', function(e) {
 $.choose_character.close();
});

//textfield character check
$.playerLabel.addEventListener('change', function(e)
{
    e.source.value = e.source.value.slice(0,5);
    switch (args.id){
		case "player1": 
		 Ti.App.Properties.setString("player1_name", e.source.value);
		break;
		
		case "player2": 
		 Ti.App.Properties.setString("player2_name", e.source.value);
		break;
		
		case "player3": 
		 Ti.App.Properties.setString("player3_name", e.source.value);
		break;
		
		case "player4":
		 Ti.App.Properties.setString("player4_name", e.source.value);
		break;
	} 
});
$.playerLabel.focus();