//parameters from previous window
var args = arguments[0] || {};

$.winner.setBackgroundColor(Ti.App.Properties.getString(args.winner+"_color", "#cccccc"));
$.winner.setBackgroundImage(Ti.App.Properties.getString(args.winner+"_face", "images/faces/face_01.png"));

function PlayAgain(e){
	var gameWindow = Alloy.createController('game').getView(); 
    gameWindow.open();
}




function NewPlayers(e){
	var players = Alloy.createController('choose_players').getView();
    players.open();
}


function Home(e){
	var home = Alloy.createController('home').getView();
    home.open();
}